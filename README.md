# Collection of scripts #
These scripts are mainly relating to [Gwyddion](http://gwyddion.net/) and how to present
the data. Normally it applies to Atomic Force Microscopy (AFM) though it can be
for any type of Scanning Probe Microscopy (SPM) or even grey scale images.

*Please treat these as work as work in progress or a brain dump*

## 3DPrint data ##
This script include a simple process as comments as the steps to follow to 
create a 3D representation of the data adquired 
## Batch reporting: Latex ##
This creates a LaTeX document with whatever is in your GWY file. 
## Batch reporting: Markdown ##
Creates a batch report that can be done in Markdown. This might be prefered as 
later can be used with pandDoc. 
