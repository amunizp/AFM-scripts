#####
#This is GPL licence (the latest version you can find normally): 
#it is a cheap copy of a script found in the gwyddion.net documentation
# pretends to be a quick way to do a batch report using LaTeX but could be done 
# with minor editing for an HTML report. 
####
# OBJECTIVE:  you have your gwy file with all the images just like you like them.
#instead of saving them one by one and then attempt to put them in your report 
# one by one we just do it all in one batch file. 
import gwyutils
# base name
basePath = "\\fpsvr2\users2$\amp\Documents\Python Scripts"
baseFile = "exported-png-%d.png"
basename = basePath + baseFile
#basename = "Documentos/gwyddion/exported-png-%d.png"
i = 0
# get list of available containers
cons = gwy.gwy_app_data_browser_get_containers()
# create tex file
texf = open(basePath +'document.tex', 'w')
# save strings for adding to the report
beginFigure = r"\begin{figure}[hbtp]"
includegraphics = r"\includegraphics[width = \textwidth]{"
caption = r"\caption{"
# iterate thru containers and datafields
for c in cons:
  # get directory of datafields where key is key in container
  dfields = gwyutils.get_data_fields_dir(c)
  for key in dfields.keys():
    # get processed datafield object
    datafield = dfields[key]
    # retrieve datafield number in container from key (for example '/0/data')
    datafield_id = key.split('/')[1]
    # set palette of datafield
    c.set_string_by_name("/%s/base/palette" % datafield_id, "Gwyddion.net")
    # invert datafield values (processing section) 
    #datafield.invert(0, 0, 1)
    # request redraw before export
    datafield.data_changed()
    # export datafield to png, for the first time show export dialog
    if i == 0:
      gwyutils.save_dfield_to_png(c, key, basename % i, gwy.RUN_INTERACTIVE)
    else :
      gwyutils.save_dfield_to_png(c, key, basename % i, gwy.RUN_NONINTERACTIVE)
    #write to report
    #texf.append(basename % i)
    print basename % i
    #textf.write("beginFigure")
    texf.write(beginFigure)
    texf.write('\n')
    texf.write(includegraphics)
    texf.write(baseFile % i)
    texf.write("}")
    texf.write('\n')
    texf.write(caption)
    texf.write(baseFile % i)
    texf.write("}")    
    texf.write('\n')
    texf.write(r'\label{fig:')
    texf.write(baseFile % i)
    texf.write("}")    
    texf.write('\n')
    texf.write("\\end{figure}")
    texf.write('\n')
    i += 1
    # request redraw datawindows with datafields
    datafield.data_changed()
texf.close()

#TODO:
# would append be better than write?
# strings as variables a little too much?
# find out how to get the meta data as a caption.
# find out how to add graphs and spectroscopies to the file. 
# more elegant way to add scripts?
# Run pdflatex from this console to have a working pdf file at the end. 
# query for path or use the path that your file is in. 
# choose tex, HTML or XML output. 
# make it a module?
# DONE: make the name of the file separate from the path to be able to use it as a caption.
# DONE: use the figure enviorment on files
